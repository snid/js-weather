let weather = {
    'apiKey': 'a045dc974e8140ed4500143dbcd3146a',
    fetchWeather: function(city){
        fetch(
            `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${this.apiKey}`
        )
        .then(response => response.json())
        .then(data => this.displayWeather(data))
        .catch(error => {
                console.log(error.message)
                document.querySelector('.weather').classList.add('no_data')
                // document.querySelector('.loading').style.content = 'Data unavailable'
            })
    },
    displayWeather: function(data) {
        const { name } = data
        const { description, icon } = data.weather[0]
        const { temp, humidity} = data.main
        const { speed } = data.wind
        console.log(data,name, temp, humidity, speed);
        document.querySelector('.city').innerText = `Weather in ${name}`         
        document.querySelector('.icon').src = `http://openweathermap.org/img/wn/${icon}.png`
        document.querySelector('.description').innerText = description
        document.querySelector('.temp').innerText = `${temp} °C`
        document.querySelector('.humidity').innerText = `Humidity: ${humidity}%`
        document.querySelector('.wind').innerText = `Wind Speed: ${speed} km/h`
        document.querySelector('.weather').classList.remove('loading')
        document.querySelector('.weather').classList.remove('no_data')
        let imgName = description.split(' ').join('-')
        console.log(imgName);
        document.querySelector('body').style.backgroundImage = `url('https://source.unsplash.com/1600x900/?${imgName}')`
        
    },
    search: function(){
        this.fetchWeather(document.querySelector('.search-bar').value)
    }
}

document.querySelector('.search button')
.addEventListener('click', function(){
    weather.search()
})

document.querySelector('.search-bar').addEventListener('keyup', e => {
    if(e.key === 'Enter')
        weather.search()
})

weather.fetchWeather('london')